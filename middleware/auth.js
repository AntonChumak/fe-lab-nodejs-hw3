const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = function(req, res, next) {
  console.log((new Date(Date.now()).toLocaleString()), req.method, req.url);
  // Get token from request header
  const authorization = req.headers.authorization;
  const token = authorization.split(' ')[1];
  if (!token) {
    console.error('Token not found, authorization failed');
    return res.status(400).json(
        {'message': 'Token not found, authorization failed'});
  }
  try {
    // verify token & extract payload
    const decoded = jwt.verify(token, config.get('jwtSecret'));
    req.user = decoded.user;
    next();
  } catch (err) {
    console.error(err.message);
    res.status(400).json({'message': 'Token is invalid'});
  }
};
