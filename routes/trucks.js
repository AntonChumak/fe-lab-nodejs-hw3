const express = require('express');
const router = new express.Router();
// const jwt = require('jsonwebtoken');
// const bcrypt = require('bcryptjs');
// const config = require('config');
const auth = require('../middleware/auth');

const User = require('../models/User');
const Truck = require('../models/Truck');

/**
* @route    GET api/trucks
* @desc     Get trucks for User
* @access   Public
**/
router.get('/', auth, async (req, res)=>{
  /* const offset = req.query.offset;
  const limit = req.query.limit;*/
  try {
    const user = await User.findById(req.user.id).select('-password -__v');
    const trucks = await Truck.find({assigned_to: user.id}).select('-__v');
    // .skip(offset).limit(limit).select('-__v');
    res.json({/* 'offset': offset,
      'limit': limit,
      'count': trucks.length,*/
      'trucks': trucks});
  } catch (err) {
    console.error(err.message);
    res.status(500).send({'message': 'Server Error'});
  }
});
/**
 * @route    POST api/trucks
 * @desc     Add Truck for User
 * @access   Public
**/
router.post('/', auth, async (req, res)=>{
  const type = req.body.type;
  // console.log(req.user.id);
  const user = await User.findById(req.user.id).select('-password -__v');
  // console.log(user);
  try {
    const truck = new Truck({
      created_by: user.id,
      assigned_to: user.id,
      type: type,
      status: 'IS'});
    await truck.save();
    res.json({'message': 'Sucsess'});
  } catch (err) {
    console.error(err.message);
    res.status(500).send({'message': 'Server Error'});
  }
});
/**
 * @route    GET api/trucks/id
 * @desc     Get a Truck for User
 * @access   Public
 **/
router.get('/:id', auth, async (req, res)=>{
  const id = req.params.id;
  try {
    const user = await User.findById(req.user.id).select('-password -__v');
    const trucks = await Truck.find({userId: user['_id'], _id: id})
        .select('-__v');
    res.json({'truck': trucks[0]});
  } catch (err) {
    console.error(err.message);
    res.status(500).send({'message': 'Server Error'});
  }
});
/**
 * @route    PUT api/trucks/id
 * @desc     Get a Truck for User
 * @access   Public
 **/
router.put('/:id', auth, async (req, res)=>{
  const type = req.body.type;
  const id = req.params.id;
  try {
    const user = await User.findById(req.user.id).select('-password -__v');
    const trucks = await Truck.find({userId: user['_id'], _id: id})
        .select('-__v');
    trucks[0].type = await type;
    await trucks[0].save();
    res.json({'message': 'Sucsess'});
  } catch (err) {
    console.error(err.message);
    res.status(500).send({'message': 'Server Error'});
  }
});
/**
 * @route    POST api/trucks/id/assign
 * @desc     Get a Truck for User
 * @access   Public
 **/
router.post('/:id/assign', auth, async (req, res)=>{
  const id = req.params.id;
  console.log(id);
  try {
    const user = await User.findById(req.user.id).select('-password -__v');
    const trucks = await Truck.findById(id).select(' -__v');
    console.log(trucks);
    // Truck.find({_id: id})
    // .select('-__v');
    trucks.assigned_to = await user.id;
    await trucks.save();
    res.json({'message': 'Truck assigned successfully'});
  } catch (err) {
    console.error(err.message);
    res.status(500).send({'message': 'Server Error'});
  }
});
/**
 * @route    DELETE api/trucks/id
 * @desc     Delete a Truck for User
 * @access   Public
 **/
router.delete('/:id', auth, async (req, res)=>{
  const id = req.params.id;
  try {
    const user = await User.findById(req.user.id).select('-password -__v');
    await Truck.deleteOne({assigned_to: user['_id'], _id: id});
    res.json({'message': 'Sucsess'});
  } catch (err) {
    console.error(err.message);
    res.status(500).send({'message': 'Server Error'});
  }
});

module.exports = router;
