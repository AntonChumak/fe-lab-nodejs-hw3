const express = require('express');
const router = new express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('config');
// const auth = require('../middleware/auth');

const User = require('../models/User');

/**
 * @route    POST api/auth/register
 * @desc     User Registration
 * @access   Public
**/
router.post('/register', async (req, res) => {
  console.log((new Date(Date.now()).toLocaleString()), req.method, req.url);
  const {email, password, role} = req.body;
  try {
    let user = await User.findOne({email});
    if (user) {
      console.error('Email already exist');
      return res.status(400).json({'message': 'Email already exist'});
    }
    user = new User({
      email,
      password,
      role});

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, salt);
    await user.save();

    const payload = {user: {id: user.id}};
    jwt.sign(payload, config.get('jwtSecret'), {
      expiresIn: 360000},
    (err) => {
      if (err) throw err;
      res.json({'message': 'Sucsess'});
    });
  } catch (err) {
    console.error(err.message);
    res.status(500).send({'message': 'Server Error'});
  }
});

/** @route    POST api/auth/login
 * @desc     Auth user and get token(login)
 * @access   Public
 * */
router.post('/login',
    async (req, res) => {
      console.log((new Date(Date.now()).toLocaleString()), req.method, req.url);
      const {email, password} = req.body;
      try {
        const user = await User.findOne({email});
        if (!user) {
          return res.status(400).json({'message': 'Invalid credentials'});
        }
        const isMatch = await bcrypt.compare(password, user.password);
        if (!isMatch) {
          return res.status(400).json({'message': 'Password invalid'});
        }
        const payload = {user: {id: user.id}};
        jwt.sign(payload, config.get('jwtSecret'),
            {expiresIn: 360000},
            (err, token) => {
              if (err) throw err;
              res.json({
                'message': 'Sucsess',
                'jwt_token': token});
            });
      } catch (err) {
        console.error(err.message);
        res.status(500).send({'message': 'Server Error'});
      };
    });

/** @route    POST api/auth/forgot_password
 * @desc     Auth user and get token(login)
 * @access   Public
 * */
router.post('/forgot_password',
    async (req, res) => {
      console.log((new Date(Date.now()).toLocaleString()), req.method, req.url);
      const {email} = req.body;
      try {
        const user = await User.findOne({email});
        if (!user) {
          return res.status(400).json({'message': 'Invalid email'});
        }
        const password = Math.floor(Math.random()*999).toString();
        console.log(password);
        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(password, salt);
        await user.save();
        const payload = {user: {id: user.id}};
        jwt.sign(payload, config.get('jwtSecret'), {
          expiresIn: 360000},
        (err) => {
          if (err) throw err;
          // send(password);
          res.json({'message': 'New password sent to your email address'});
        });
      } catch (err) {
        console.error(err.message);
        res.status(500).send({'message': 'Server Error'});
      };
    });

module.exports = router;
