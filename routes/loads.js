const express = require('express');
const router = new express.Router();
// const jwt = require('jsonwebtoken');
// const bcrypt = require('bcryptjs');
// const config = require('config');
const auth = require('../middleware/auth');

const User = require('../models/User');
const Load = require('../models/Load');

/**
* @route    GET api/loads
* @desc     Get loads for User
* @access   Public
**/
router.get('/', auth, async (req, res)=>{
  const offset = req.query.offset;
  const limit = req.query.limit;
  try {
    const user = await User.findById(req.user.id).select('-password -__v');
    const loads = await Load.find({userId: user['_id']})
        .skip(offset).limit(limit).select('-__v');
    res.json({'loads': loads});
  } catch (err) {
    console.error(err.message);
    res.status(500).send({'message': 'Server Error'});
  }
});
/**
 * @route    POST api/loads
 * @desc     Add Load for User
 * @access   Public
**/
router.post('/', auth, async (req, res)=>{
  // eslint-disable-next-line camelcase
  const {delivery_address,
  // eslint-disable-next-line camelcase
    pickup_address,
    payload,
    name,
    dimensions} = req.body;
  const user = await User.findById(req.user.id).select('-password -__v');
  try {
    const load = new Load({
      created_by: user.id,
      assigned_to: user.id,
      status: 'NEW',
      state: 'En route to Pick Up',
      // eslint-disable-next-line camelcase
      delivery_address: delivery_address,
      // eslint-disable-next-line camelcase
      pickup_address: pickup_address,
      payload: payload,
      name: name,
      dimensions: dimensions,
      logs: {message: 'En route to Pick Up',
        date: (new Date(Date.now()).toLocaleString())}});
    await load.save();
    res.json({'message': 'Sucsess'});
  } catch (err) {
    console.error(err.message);
    res.status(500).send({'message': 'Server Error'});
  }
});
/**
* @route    GET api/loads/active
* @desc     Get loads for User
* @access   Public
**/
router.get('/', auth, async (req, res)=>{
  try {
    const user = await User.findById(req.user.id).select('-password -__v');
    const loads = await Load.find({userId: user['_id']})
        .skip(offset).limit(limit).select('-__v');
    res.json(loads);
  } catch (err) {
    console.error(err.message);
    res.status(500).send({'message': 'Server Error'});
  }
});
/**
 * @route    PATCH api/loads/id
 * @desc     Get a Load for User
 * @access   Public
 **/
router.patch('/active/state', auth, async (req, res)=>{
  try {
    const user = await User.findById(req.user.id).select('-password -__v');
    const loads = await Load.find({userId: user['_id'], _id: id})
        .select('-__v');
    loads[0].state = '!loads[0].state';
    await loads[0].save();
    // eslint-disable-next-line quotes
    res.json({'message': "Load state changed to 'En route to Delivery'"});
  } catch (err) {
    console.error(err.message);
    res.status(500).send({'message': 'Server Error'});
  }
});
/**
 * @route    GET api/loads/id
 * @desc     Get a Load for User
 * @access   Public
 **/
router.get('/:id', auth, async (req, res)=>{
  const id = req.params.id;
  try {
    const user = await User.findById(req.user.id).select('-password -__v');
    const loads = await Load.find({assigned_to: user['_id'], _id: id})
        .select('-__v');
    res.json({'note': loads[0]});
  } catch (err) {
    console.error(err.message);
    res.status(500).send({'message': 'Server Error'});
  }
});
/**
 * @route    PUT api/loads/id
 * @desc     Get a Load for User
 * @access   Public
 **/
router.put('/:id', auth, async (req, res)=>{
  const text = req.body.text;
  const id = req.params.id;
  try {
    const user = await User.findById(req.user.id).select('-password -__v');
    const loads = await Load.find({userId: user['_id'], _id: id})
        .select('-__v');
    loads[0].text = await text;
    await loads[0].save();
    res.json({'message': 'Load details changed successfully'});
  } catch (err) {
    console.error(err.message);
    res.status(500).send({'message': 'Server Error'});
  }
});
/**
 * @route    DELETE api/loads/id
 * @desc     Get a Load for User
 * @access   Public
 **/
router.delete('/:id', auth, async (req, res)=>{
  const id = req.params.id;
  try {
    const user = await User.findById(req.user.id).select('-password -__v');
    await Load.deleteOne({userId: user['_id'], _id: id});
    res.json({'message': 'Sucsess'});
  } catch (err) {
    console.error(err.message);
    res.status(500).send({'message': 'Server Error'});
  }
});
/**
 * @route    POST api/loads/id
 * @desc     Get a Load for User
 * @access   Public
 **/
router.patch('/:id/post', auth, async (req, res)=>{
  const id = req.params.id;
  try {
    const user = await User.findById(req.user.id).select('-password -__v');
    const loads = await Load.find({userId: user['_id'], _id: id})
        .select('-__v');
    loads[0].completed = !loads[0].completed;
    await loads[0].save();
    res.json({'message': 'Load posted successfully',
      'driver_found': true});
  } catch (err) {
    console.error(err.message);
    res.status(500).send({'message': 'Server Error'});
  }
});
/**
 * @route    POST api/loads/id
 * @desc     Get a Load for User
 * @access   Public
 **/
router.patch('/:id/shipping_info', auth, async (req, res)=>{
  const id = req.params.id;
  try {
    const user = await User.findById(req.user.id).select('-password -__v');
    const loads = await Load.find({userId: user['_id'], _id: id})
        .select('-__v');
    loads[0].completed = !loads[0].completed;
    await loads[0].save();
    res.json({'message': 'Load posted successfully',
      'driver_found': true});
  } catch (err) {
    console.error(err.message);
    res.status(500).send({'message': 'Server Error'});
  }
});
module.exports = router;
