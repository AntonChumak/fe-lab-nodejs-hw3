const express = require('express');
const router = new express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('config');
const auth = require('../middleware/auth');

const User = require('../models/User');

/**
* @route    GET api/users
* @desc     Get logged in user profile
* @access   Private
**/
router.get('/me', auth, async (req, res)=>{
  try {
    const user = await User.findById(req.user.id).select('-password -__v');
    res.json(user);
  } catch (err) {
    console.error(err.message);
    res.status(500).send({'message': 'Internal server error'});
  }
});
/**
* @route    DELETE api/users
* @desc     Delete logged in user
* @access   Private
**/
router.delete('/me', auth, async (req, res)=>{
  try {
    await User.deleteOne({_id: req.user.id});
    res.json({'message': 'Success'});
  } catch (err) {
    console.error(err.message);
    res.status(500).send({'message': 'Internal server error'});
  }
});
/**
 * @route    PATCH api/users/password
 * @desc     Change logged in user password
 * @access   Private
**/
router.patch('/password', auth, async (req, res)=>{
  const {oldPassword, newPassword} = req.body;
  const user = await User.findById(req.user.id).select('-__v');
  try {
    if (! await bcrypt.compare(oldPassword, user.password)) {
      console.log('Invalid old password');
      res.status(400).json({'message': 'Invalid old password'});
      return;
    }
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(newPassword, salt);
    await user.save();
    const payload = {user: {id: user.id}};
    jwt.sign(payload, config.get('jwtSecret'), {
      expiresIn: 360000},
    (err) => {
      if (err) throw err;
      res.json({
        'message': 'Success'});
    });
  } catch (err) {
    console.error(err.message);
    res.status(500).send({'message': 'Internal server error'});
  };
});

module.exports = router;
