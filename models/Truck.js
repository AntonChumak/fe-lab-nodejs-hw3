const mongoose = require('mongoose');

const TruckSchema = new mongoose.Schema({
  created_by: {
    type: String,
    required: true},
  assigned_to: {
    type: String,
    required: true},
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
    required: true},
  status: {
    type: String,
    enum: ['OL', 'IS'],
    required: true},
  createdDate: {
    type: Date, default: Date.now}});

module.exports = mongoose.model('truck', TruckSchema);
