const mongoose = require('mongoose');

/* const DimensionsSchema = new mongoose.Schema({
  width: Number,
  length: Number,
  height: Number
});*/
const LogSchema = new mongoose.Schema({
  message: String,
  date: String});

const LoadSchema = new mongoose.Schema({
  created_by: {
    type: String,
    required: true},
  assigned_to: {
    type: String,
    required: true},
  status: {
    type: String,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    required: true},
  state: {
    type: String,
    enum: ['En route to Pick Up', 'Arrived to Pick Up',
      'En route to delivery', 'Arrived to delivery'],
    required: true},
  name: {
    type: String,
    required: true},
  payload: {
    type: Number,
    required: true},
  pickup_address: {
    type: String,
    required: true},
  delivery_address: {
    type: String,
    required: true},
  dimensions: {
    type: Object,
    width: Number,
    length: Number,
    height: Number,
    required: true},
  logs: {
    type: [LogSchema],
    // required: true},
  },
  createdDate: {
    type: Date, default: Date.now}});

module.exports = mongoose.model('load', LoadSchema);
