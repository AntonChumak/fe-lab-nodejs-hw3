const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  // username: {
  //   type: String,
  //   required: true,
  //   unique: true},
  email: {
    type: String,
    required: true},
  password: {
    type: String,
    required: true},
  role: {
    type: String,
    enum: ['DRIVER', 'SHIPPER'],
    required: true},
  createdDate: {
    type: Date,
    default: Date.now}});

module.exports = mongoose.model('user', UserSchema);
